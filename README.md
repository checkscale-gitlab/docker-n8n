![](https://images.microbadger.com/badges/version/jarylc/n8n.svg) ![](https://images.microbadger.com/badges/image/jarylc/n8n.svg) ![](https://img.shields.io/docker/stars/jarylc/n8n.svg) ![](https://img.shields.io/docker/pulls/jarylc/n8n.svg)

[Advanced Configuration](https://github.com/n8n-io/n8n/blob/master/docker/images/n8n/README.md)

Comes with automated NPM installing of additional modules before start (using ADDITIONAL_MODULES env variable) from 0.120.0!

# Environment variables:
| Environment | Default value | Description
|---|---|---|
| UID | 1000 | User ID to run N8N as |
| GID | 1000 | Group ID to run N8N as |
| TZ  | UTC | Timezone |
| GENERIC_TIMEZONE | UTC | N8N Timezone |
| ADDITIONAL_MODULES | ws | Additional modules to npm install, separated by spaces (" "). |

# Volumes
- /data - n8n data

# Deploying
## Terminal
```bash
docker run -d \
    --name n8n \
    -e UID=1000 \
    -e GID=1000 \
    -e TZ=UTC \
    -e GENERIC_TIMEZONE=UTC \
    -e ADDITIONAL_MODULES=ws \
    -p 5678:5678 \
    -v /path/data:/data \
    --restart unless-stopped \
    minimages/n8n
```
## Docker-compose
```yml
n8n:
    image: minimages/n8n
    ports:
        - "5678:5678"
    volumes:
        - /path/data:/data
    environment:
        - UID=1000
        - GID=1000
        - TZ=UTC
        - GENERIC_TIMEZONE=UTC
        - ADDITIONAL_MODULES=ws
    restart: unless-stopped
```
